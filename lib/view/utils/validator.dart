class Validator{
  static String? validateEmail(String value){
    if (value.isEmpty)return'Enter Email';
    RegExp regex = new RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    if(!regex.hasMatch(value)) {
      return 'Enter Valid Email';
    }
      return null;
  }
}