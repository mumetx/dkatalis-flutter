import 'dart:typed_data';

import 'package:flutter/material.dart';

class DkScaffold extends StatelessWidget {
  final Key? scaffoldKey;
  final Widget body;
  final AppBar? appBar;
  final FloatingActionButton? floatingActionButton;

  static Uint8List? headerImage;

  DkScaffold(
      {required this.body,
      this.scaffoldKey,
      this.appBar,
      this.floatingActionButton});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: appBar,
      floatingActionButton: floatingActionButton,
      body: Stack(
        children: [SafeArea(child: this.body)],
      ),
    );
  }
}
