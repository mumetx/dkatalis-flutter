import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DkCustomInputField extends StatelessWidget {
  final void Function(String) onChanged;
  final void Function(String)? onSaved;

  const DkCustomInputField({Key? key, this.onSaved, required this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      onSaved: (newValue) => onSaved?.call(newValue ?? ""),
      decoration: InputDecoration(
          hintStyle: TextStyle(
            color: Colors.grey[400],
          ),
          fillColor: Colors.white70,
          filled: true,
          contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
          border: _border(),
          focusedBorder: _border(),
          disabledBorder: _border()),
    );
  }

  _border() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
      borderSide: BorderSide(
        color: Color(0xffB3ABAB),
        width: 1.0,
      ),
    );
  }
}
