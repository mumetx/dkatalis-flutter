import 'package:dkatalis_test/util/assets.dart';
import 'package:dkatalis_test/util/router.dart';
import 'package:dkatalis_test/view/pages/onboarding/createaccount/input_personal_page.dart';
import 'package:dkatalis_test/view/utils/theme_config.dart';
import 'package:dkatalis_test/view/widgets/DkCustomInputField.dart';
import 'package:dkatalis_test/view/widgets/DkScaffold.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputPasswordPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _InputPasswordPageState();
  }
}

class _InputPasswordPageState extends State<InputPasswordPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool _lower = false;
  bool _upper = false;
  bool _number = false;
  bool _max = false;
  bool _obscureTextNew = true;

  final _textNew = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return DkScaffold(
      appBar: AppBar(
        backgroundColor: ThemeConfig.lightAccent,
        title: Text(
          'Create Account',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
        ),
        elevation: 0,
      ),
      body: Container(
        color: ThemeConfig.lightAccent,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _roundedButton(),
              SizedBox(height: 70.0),
              Text(
                "Create Password",
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .apply(color: Colors.white),
              ),
              Text("Password will be used to login to account",
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .apply(color: Colors.white)),
              SizedBox(height: 70.0),
              Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: formKey,
                child: _buildForm(),
              ),
              SizedBox(height: 30.0),
              _ChangeButton()
            ],
          ),
        ),
      ),
    );
  }

  _checkButton() {
    if (_textNew.text.isEmpty) {
      setState(() {
        _validate = false;
      });
    } else {
      setState(() {
        _validate = true;
      });
    }
  }

  static final RegExp lowerRegExp = RegExp('[a-z]');
  static final RegExp upperRegExp = RegExp("[A-Z]");
  static final RegExp numberRegExp = RegExp("[0-9]");

  bool lowerCase(String str) {
    return lowerRegExp.hasMatch(str);
  }

  bool upperCase(String str) {
    return upperRegExp.hasMatch(str);
  }

  bool numberCase(String str) {
    return numberRegExp.hasMatch(str);
  }

  var stingIsi = "";
  _buildForm() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: _textNew,
              obscureText: _obscureTextNew,
              enableSuggestions: false,
              autocorrect: false,
              onChanged: (value) async {
                if (lowerCase(_textNew.text)) {
                  _lower = true;
                }
                if (upperCase(_textNew.text)) {
                  _upper = true;
                }
                if (numberCase(_textNew.text)) {
                  _number = true;
                }
                if (_textNew.text.length >= 9) {
                  _max = true;
                }
                print(_textNew.text.length);
                _checkButton();
              },
              decoration: InputDecoration(
                hintText: "Create Password",
                filled: true,
                fillColor: Colors.white,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.lightBlueAccent, width: 2.0),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                suffixIcon: IconButton(
                    icon: new Image.asset(
                        !_obscureTextNew ? Assets.invisible : Assets.visible),
                    onPressed: () {
                      setState(() {
                        _obscureTextNew = !_obscureTextNew;
                      });
                    }),
              ),
            ),
            SizedBox(height: 20.0),
            Row(
              children: [
                Text("Complexity:"),
                _upper && _lower
                    ? Text(
                        " Strong",
                        style: TextStyle(color: Colors.lightGreenAccent[700]),
                      )
                    : Text(
                        " weak",
                        style: TextStyle(color: Colors.orange),
                      ),
              ],
            ),
            SizedBox(height: 50.0),
            Row(
              children: [_checkPassword()],
            )
          ],
        ),
      ],
    );
  }

  _checkPassword() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            Container(
              height: 4.0,
              width: 10.0,
            ),
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _lower
                      ? new Icon(
                          Icons.check,
                        )
                      : new Text("a",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center),
                  new Text("Lowercase",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 60.0,
            ),
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _upper
                      ? new Icon(
                          Icons.check,
                        )
                      : new Text("A",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center),
                  new Text("Uppercase",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 60.0,
            ),
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _number
                      ? new Icon(
                          Icons.check,
                        )
                      : new Text("123",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center),
                  new Text("Number",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 60.0,
            ),
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _max
                      ? new Icon(
                          Icons.check,
                        )
                      : new Text("9+",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center),
                  new Text("Character",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  _roundedButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.green[800], // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("1",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("2",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("3",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("4",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  _ChangeButton() {
    return SizedBox(
      width: double.infinity,
      height: 40.0,
      child: new ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: _validate ? Colors.indigo[600] : Colors.indigo[200],
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
        ),
        onPressed: _validate
            ? () {
                Navigate.pushPage(context, InputPersonalPage());
              }
            : null,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text("Next",
                style: TextStyle(
                  color: _validate ? Colors.white : Colors.grey[400],
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}
