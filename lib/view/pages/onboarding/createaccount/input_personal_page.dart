import 'package:dkatalis_test/util/router.dart';
import 'package:dkatalis_test/view/pages/onboarding/createaccount/input_datetime_page.dart';
import 'package:dkatalis_test/view/utils/theme_config.dart';
import 'package:dkatalis_test/view/widgets/DkCustomInputField.dart';
import 'package:dkatalis_test/view/widgets/DkScaffold.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputPersonalPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _InputPersonalPageState();
  }
}

class _InputPersonalPageState extends State<InputPersonalPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return DkScaffold(
      appBar: AppBar(
        backgroundColor: ThemeConfig.lightAccent,
        title: Text(
          'Create Account',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
        ),
        elevation: 0,
      ),
      body: Container(
        color: ThemeConfig.lightAccent,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _roundedButton(),
              SizedBox(height: 70.0),
              Text(
                "Personal Information",
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .apply(color: Colors.white),
              ),
              Text(
                  "Please fill in the information below and your goal for digital saving.",
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .apply(color: Colors.white)),
              SizedBox(height: 70.0),
              Form(
                child: _Goal(),
              ),
              SizedBox(height: 10.0),
              Form(
                child: _Income(),
              ),
              SizedBox(height: 10.0),
              Form(
                child: _Expense(),
              ),
              SizedBox(height: 30.0),
              _ChangeButton(),
            ],
          ),
        ),
      ),
    );
  }

  List _List = [
    "-Choose Option-",
    "A",
    "B",
    "C",
    "D",
  ];
  var _valueA = "-Choose Option-";
  var _valueB = "-Choose Option-";
  var _valueC = "-Choose Option-";
  _Goal() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width / 0.25,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  color: Colors.white,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                "Goal for activation",
                                textAlign: TextAlign.left,
                                style:
                                    TextStyle(fontSize: 13, color: Colors.grey),
                              ),
                            )),
                        DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            alignedDropdown: true,
                            child: DropdownButton(
                              elevation: 2,
                              dropdownColor: Colors.white,
                              focusColor: Colors.white,
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: ThemeConfig.darkPrimary,
                              ),
                              value: _valueA,
                              isDense: false,
                              onChanged: (String? newValue) {
                                setState(() {
                                  if (newValue != null) {
                                    _valueA = newValue;
                                    _checkButton();
                                  }
                                });
                              },
                              hint: Text("-Choose Option-",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w500)),
                              items: _List.map((key) {
                                return DropdownMenuItem<String>(
                                  value: key.toString(),
                                  child: Text(
                                    key.toString(),
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        )
                      ]))),
        ],
      ),
    );
  }

  _Income() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width / 0.25,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  color: Colors.white,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              "Monthly Income",
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(fontSize: 13, color: Colors.grey),
                            ),
                          ),
                        ),
                        DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            alignedDropdown: true,
                            child: DropdownButton(
                              elevation: 2,
                              dropdownColor: Colors.white,
                              focusColor: Colors.white,
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: ThemeConfig.darkPrimary,
                              ),
                              value: _valueB,
                              isDense: false,
                              onChanged: (String? newValue) {
                                setState(() {
                                  if (newValue != null) {
                                    _valueB = newValue;
                                    _checkButton();
                                  }
                                });
                              },
                              hint: Text("-Choose Option-",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w500)),
                              items: _List.map((key) {
                                return DropdownMenuItem<String>(
                                  value: key.toString(),
                                  child: Text(
                                    key.toString(),
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        )
                      ]))),
        ],
      ),
    );
  }

  _Expense() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width / 0.25,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  color: Colors.white,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                "Monthly Expense",
                                textAlign: TextAlign.left,
                                style:
                                    TextStyle(fontSize: 13, color: Colors.grey),
                              ),
                            )),
                        DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            alignedDropdown: true,
                            child: DropdownButton(
                              elevation: 2,
                              dropdownColor: Colors.white,
                              focusColor: Colors.white,
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: ThemeConfig.darkPrimary,
                              ),
                              value: _valueC,
                              isDense: false,
                              onChanged: (String? newValue) {
                                setState(() {
                                  if (newValue != null) {
                                    _valueC = newValue;
                                    _checkButton();
                                  }
                                });
                              },
                              hint: Text("-Choose Option-",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w500)),
                              items: _List.map((key) {
                                return DropdownMenuItem<String>(
                                  value: key.toString(),
                                  child: Text(
                                    key.toString(),
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        )
                      ]))),
        ],
      ),
    );
  }

  _checkButton() {
    if (_valueA == "-Choose Option-" ||
        _valueB == "-Choose Option-" ||
        _valueC == "-Choose Option-") {
      setState(() {
        _validate = false;
      });
    } else {
      setState(() {
        _validate = true;
      });
    }
  }

  _roundedButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.green[800], // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("1",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.green[800], // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("2",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("3",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("4",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  _ChangeButton() {
    return SizedBox(
      width: double.infinity,
      height: 40.0,
      child: new ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: _validate ? Colors.indigo[600] : Colors.indigo[200],
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
        ),
        onPressed: _validate
            ? () {
                Navigate.pushPage(context, InputDateTimePage());
              }
            : null,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text("Next",
                style: TextStyle(
                  color: _validate ? Colors.white : Colors.grey[400],
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}
