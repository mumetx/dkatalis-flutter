import 'package:dkatalis_test/util/assets.dart';
import 'package:dkatalis_test/view/pages/onboarding/create_account_page.dart';
import 'package:dkatalis_test/view/pages/onboarding/createaccount/input_password_page.dart';
import 'package:dkatalis_test/view/utils/validator.dart';
import 'package:dkatalis_test/view/utils/theme_config.dart';
import 'package:dkatalis_test/view/widgets/DkScaffold.dart';
import 'package:dkatalis_test/util/router.dart';
import 'package:flutter/material.dart';

class OnBoardingPage extends StatefulWidget {
  OnBoardingPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OnBoardingPage();
}

class _OnBoardingPage extends State<OnBoardingPage> {
  bool _validate = false;
  bool _obscureTextNew = true;

  final _textNew = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return DkScaffold(
      appBar: AppBar(
          toolbarHeight: 200,
          elevation: 0,
          flexibleSpace: Container(
              child:
                  Column(children: [SizedBox(height: 70.0), _roundedButton()]),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                      fit: BoxFit.fill, image: AssetImage(Assets.appLogo)),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFF000000).withAlpha(60),
                      blurRadius: 6.0,
                      spreadRadius: 0.0,
                      offset: Offset(
                        0.0,
                        3.0,
                      ),
                    ),
                  ]))),
      body: Container(
        color: ThemeConfig.lightPrimary,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 70.0),
              Text(
                "Welcome to GIN ",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 35.0,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                "Finans",
                style: TextStyle(
                    color: Colors.lightBlue,
                    fontSize: 35.0,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                  "Welcome to The Bank of the Future. Manage and track your accounts on the go.",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500)),
              SizedBox(height: 70.0),
              Form(
                child: _buildForm(),
              ),
              SizedBox(height: 30.0),
              _ChangeButton()
            ],
          ),
        ),
      ),
    );
  }

  _checkButton() {
    if (_textNew.text.isEmpty) {
      setState(() {
        _validate = false;
      });
    } else {
      setState(() {
        _validate = true;
      });
    }
  }

  static final RegExp emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  bool emailCase(String str) {
    return emailRegExp.hasMatch(str);
  }

  _buildForm() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              controller: _textNew,
              enableSuggestions: false,
              autocorrect: false,
              onChanged: (value) async {
                if (emailCase(_textNew.text)) {
                  _checkButton();
                }
              },
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.lightBlueAccent, width: 2.0),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                hintText: "Email",
                prefixIcon: Icon(Icons.mail),
              ),
            ),
          ],
        ),
      ],
    );
  }

  _roundedButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            SizedBox(width: 10.0),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("1",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 42.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("3",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 42.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("2",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 42.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("4",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  _ChangeButton() {
    return SizedBox(
      width: double.infinity,
      height: 40.0,
      child: new ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: _validate ? Colors.indigo[600] : Colors.indigo[200],
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
        ),
        onPressed: _validate
            ? () {
                Navigate.pushPage(context, InputPasswordPage());
              }
            : null,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text("Next",
                style: TextStyle(
                  color: _validate ? Colors.white : Colors.grey[400],
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}
