import 'package:dkatalis_test/util/router.dart';
import 'package:dkatalis_test/view/utils/theme_config.dart';
import 'package:dkatalis_test/view/widgets/DkCustomInputField.dart';
import 'package:dkatalis_test/view/widgets/DkScaffold.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class InputDateTimePage extends StatefulWidget {
  const InputDateTimePage({Key? key, this.restorationId}) : super(key: key);

  final String? restorationId;

  @override
  State<StatefulWidget> createState() {
    return _InputDateTimePageState();
  }
}

class _InputDateTimePageState extends State<InputDateTimePage>
    with RestorationMixin, SingleTickerProviderStateMixin {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _validate = false;

  @override
  String? get restorationId => widget.restorationId;
  TimeOfDay selectedTime = TimeOfDay.now();

  final RestorableDateTime _selectedDate = RestorableDateTime(DateTime.now());
  late final RestorableRouteFuture<DateTime?> _restorableDatePickerRouteFuture =
      RestorableRouteFuture<DateTime?>(
    onComplete: _selectDate,
    onPresent: (NavigatorState navigator, Object? arguments) {
      return navigator.restorablePush(
        _datePickerRoute,
        arguments: _selectedDate.value.millisecondsSinceEpoch,
      );
    },
  );

  static Route<DateTime> _datePickerRoute(
    BuildContext context,
    Object? arguments,
  ) {
    return DialogRoute<DateTime>(
      context: context,
      builder: (BuildContext context) {
        return DatePickerDialog(
          restorationId: 'date_picker_dialog',
          initialEntryMode: DatePickerEntryMode.calendarOnly,
          initialDate: DateTime.fromMillisecondsSinceEpoch(arguments! as int),
          firstDate: DateTime(2021, 1, 1),
          lastDate: DateTime(2022, 1, 1),
        );
      },
    );
  }

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_selectedDate, 'selected_date');
    registerForRestoration(
        _restorableDatePickerRouteFuture, 'date_picker_route_future');
  }

  var _value = "-Choose Option-";
  var _valueTime = "-Choose Option-";

  void _selectDate(DateTime? newSelectedDate) {
    if (newSelectedDate != null) {
      setState(() {
        // _value= '${_selectedDate.value}';
        _selectedDate.value = newSelectedDate;
        _value = DateFormat('EEEE, dd MMM yyyy').format(_selectedDate.value);
        _checkButton();
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked_s = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );

    if (picked_s != null && picked_s != selectedTime)
      setState(() {
        selectedTime = picked_s;
        _valueTime = "${selectedTime.hour}:${selectedTime.minute}";
        _checkButton();
      });
  }

  late AnimationController _animationController;
  late Animation _animation;

  @override
  void initState() {
    // TODO: implement initState
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat(reverse: true);
    _animation = Tween(begin: 2.0, end: 12.0).animate(_animationController)
      ..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  Widget build(BuildContext context) {
    return DkScaffold(
      appBar: AppBar(
        backgroundColor: ThemeConfig.lightAccent,
        title: Text(
          'Create Account',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
        ),
        elevation: 0,
      ),
      body: Container(
        color: ThemeConfig.lightAccent,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _roundedButton(),
              SizedBox(height: 30.0),
              Container(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Container(
                    child: Icon(Icons.date_range, size: 30, color: Colors.blue),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.white,
                              blurRadius: _animation.value,
                              spreadRadius: _animation.value)
                        ]),
                  ),
                ),
              ),
              SizedBox(height: 30.0),
              Text(
                "Schedule Video Call",
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .apply(color: Colors.white),
              ),
              Text(
                  "Choose teh date and time that you preferred, we wills end a link via email to make a video call on the scheduled date and time",
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .apply(color: Colors.white)),
              SizedBox(height: 70.0),
              Form(
                child: _Date(),
              ),
              SizedBox(height: 10.0),
              Form(
                child: _Time(),
              ),
              SizedBox(height: 30.0),
              _ChangeButton()
            ],
          ),
        ),
      ),
    );
  }

  _Date() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width / 0.25,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  color: Colors.white,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                "Date",
                                textAlign: TextAlign.left,
                                style:
                                    TextStyle(fontSize: 13, color: Colors.grey),
                              ),
                            )),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                  onPressed: () {
                                    _restorableDatePickerRouteFuture.present();
                                  },
                                  color: Colors.white,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(_value),
                                      new Icon(Icons.keyboard_arrow_down),
                                    ],
                                  )),
                            ],
                          ),
                        )
                      ]))),
        ],
      ),
    );
  }

  _Time() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width / 0.25,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  color: Colors.white,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              "Time",
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(fontSize: 13, color: Colors.grey),
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                  onPressed: () {
                                    _selectTime(context);
                                  },
                                  color: Colors.white,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(_valueTime),
                                      new Icon(Icons.keyboard_arrow_down),
                                    ],
                                  )),
                            ],
                          ),
                        )
                      ]))),
        ],
      ),
    );
  }

  _checkButton() {
    if (_value == "-Choose Option-" || _valueTime == "-Choose Option-") {
      setState(() {
        _validate = false;
      });
    } else {
      setState(() {
        _validate = true;
      });
    }
  }

  _roundedButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.green[800], // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("1",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.green[800], // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("2",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.green[800], // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("3",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              height: 4.0,
              width: 35.0,
              color: Colors.black,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                side: BorderSide(
                  width: 1.0,
                  color: Colors.black,
                ),
                shape: CircleBorder(),
                padding: EdgeInsets.all(13),
                primary: Colors.white, // <-- Button color
              ),
              onPressed: () {},
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("4",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  _ChangeButton() {
    return SizedBox(
      width: double.infinity,
      height: 40.0,
      child: new ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: _validate ? Colors.indigo[600] : Colors.indigo[200],
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
        ),
        onPressed: _validate ? () {} : null,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text("Next",
                style: TextStyle(
                  color: _validate ? Colors.white : Colors.grey[400],
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}
