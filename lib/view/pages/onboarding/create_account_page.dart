import 'package:dkatalis_test/view/pages/onboarding/createaccount/input_datetime_page.dart';
import 'package:dkatalis_test/view/pages/onboarding/createaccount/input_password_page.dart';
import 'package:dkatalis_test/view/pages/onboarding/createaccount/input_personal_page.dart';
import 'package:dkatalis_test/view/utils/theme_config.dart';
import 'package:dkatalis_test/view/widgets/DkScaffold.dart';
import 'package:flutter/material.dart';

class CreateAccountPage extends StatefulWidget {
  CreateAccountPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OnBoardingPage();
}

class _OnBoardingPage extends State<CreateAccountPage> {
  int _currentStep = 0;

  @override
  Widget build(BuildContext context) {
    return DkScaffold(
      appBar: AppBar(
        backgroundColor: ThemeConfig.lightAccent,
        title: Text(
          'Create Account',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
        ),
        elevation: 0,
      ),
      body: Container(
        color: ThemeConfig.lightAccent,
        child: Theme(
          data: Theme.of(context).copyWith(
              canvasColor: Colors.transparent, shadowColor: Colors.transparent),
          child: Stepper(
            controlsBuilder: (BuildContext context,
                {VoidCallback? onStepContinue, VoidCallback? onStepCancel}) {
              return Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.85,
                    child: TextButton(
                      onPressed: onStepContinue,
                      child: const Text(
                        'Next',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 13),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.indigo[200], // background
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
            type: StepperType.horizontal,
            physics: ScrollPhysics(),
            currentStep: _currentStep,
            onStepContinue: _continued,
            onStepTapped: (step) => _tapped(step),
            steps: _steps,
          ),
        ),
      ),
    );
  }

  late List<Step> _steps = [
    Step(title: SizedBox(), content: InputPasswordPage()),
    Step(title: SizedBox(), content: InputPersonalPage()),
    Step(title: SizedBox(), content: InputDateTimePage()),
    Step(title: SizedBox(), content: SizedBox()),
  ];

  _continued() {
    _currentStep < 2 ? setState(() => _currentStep += 1) : null;
  }

  _tapped(int step) {
    setState(() => _currentStep = step);
  }
}
