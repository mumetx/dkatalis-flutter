import 'package:dkatalis_test/view/pages/onboarding/createaccount/on_boarding_page.dart';
import 'package:dkatalis_test/view/utils/theme_config.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  // This widgets is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeConfig.lightTheme,
      home: OnBoardingPage(),
    );
  }
}
