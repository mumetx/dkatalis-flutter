class Assets {
  Assets._();

  // assets
  static const String appLogo = "assets/images/header_bg.png";
  static const String visible = "assets/images/visible_icon.png";
  static const String invisible = "assets/images/eye_view.png";

}
